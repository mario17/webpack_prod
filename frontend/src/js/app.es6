//Pasando parametros al constructor
/*class Project  {
	constructor(name) {
	 console.log("constructor here:  "+ name);
	}
}

class softwareProject extends Project {

}

let p = new softwareProject("Lima");
console.log(p);*/
// extends crear una subclase a partir de clases personalizadas, así como sus objetos incorporados
// interactuando con dos clases 
/*class Project  {
	constructor() {
	 	console.log("constructor here:  ");
	}
}*/

/*class softwareProject extends Project {
	constructor() {
		//super(); es necesario a la hora de extender
		//si no se referencia en la clase que esta extendiendo sale un error de contexto, ya que no sabe a que constructor obedecer
		//super es usada para llamar funciones de un objeto padre. 
	 	console.log("another constructor here:  ");
	}
}

let p = new softwareProject();
console.log(p); */

//example 3
//using methods

/*class  Project  {
	getTask() {
	 	return 50 ;
	}
}
class ProjectSoftware extends Project {
	getTask() {
	 	return super.getTask() + 6 ;
	}
}

let  p = new ProjectSoftware();
console.log(p.getTask());*/
//50

//example 4 

/*let  Project  = {
	getTask() {
	 	return 50 ;
	}
}
let ProjectSoftware ={
	getTask() {
	 	return super.getTask() + 6 ;
	}
}

Object.setPrototypeOf(ProjectSoftware, Project)
console.log(ProjectSoftware.getTask());*/

//56

//example 5  
/*
	Properties for class instances
*/
/*class Project  {
	constructor() {
		this.location = "Lima"
	}

}

class softwareProject extends Project {
	constructor() {
		super();
	}

}
let p = new softwareProject();
console.log(p.location);*/
//lima 
// se ṕueden utilziar  variables definidas en el constructor padre

//Example 6 
//same example with let 

/*class Project  {
	constructor() {
		let location = "Lima"
	}

}

class softwareProject extends Project {
	constructor() {
		super();
	}

}
let p = new softwareProject();
console.log(p.location);*/

//no toma el  contexto 
//undefined
//example 7 
//concatenando 

/*class Project  {
	constructor() {
		this.location = "Lima "
	}

}

class softwareProject extends Project {
	constructor() {
		super();
		this.location = this.location + "beauty"
	}

}
let p = new softwareProject();
console.log(p.location);*/

/*
   CLASSES : STATIC MEMBERS

*/

/*class Project  {
	// methods
	static getDefaultId(){
		return 0 ;
	}
	//static solo funciona dentro de las clases y en los metodos.
}

console.log(Project.getDefaultId());
*/

/*
   CLASSES : NEW TARGET

*/

/*class Project  {
	constructor(args) {
		// code
		console.log(new.target);//es una function
		//hace referencia al constructor directo
	}

	// methods
}
let p = new Project();*/

/*class Project  {
	constructor(args) {
		console.log(new.target , "log");//es una function
	}
}

class softProject extends Project {


	// methods
}
let p = new softProject();
/*constructor(args) {
		super();
	}*/ 

/*class Project {
	constructor() {
		console.log(new.target.getDefault());
	}
}

class softwareProject extends Project {
	static getDefault(){return 99;}
}

let p = new softwareProject();*/
import * as symbols from "./symbols.es6";
import * as well_known from "./well-known-symbols.es6";
import * as object_extension from "./object-extensions.es6";
import * as string_extension from "./string_extensions.es6";
import * as string_extension from "./number_extensions.es6";
//console.log(symbols);
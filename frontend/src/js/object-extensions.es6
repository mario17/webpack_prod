/* 
	Object  extension
*/

/* Example 01*/

/*let a = {
	x:1
};
let b = {
	y:2
}
Object.setPrototypeOf(a,b);

console.log(a.y);2 */ 
//crea un prototype dentro de a con el valor de b 
/*example 02*/
/*let a = {a:10};
let b = {b:2,a:11}
let target = {};
Object.assign(target, a,b);//segundo y tercer parametro define el orden, en este caso a a se trae lo de b,lo sobreescribe
console.log(target); */// Object {a: 1, b: 2}
/*El método Object.assign() es usado para copiar los valores de todas la propiedades propias enumerables de uno o más objetos */
/*s i la propiedad es la misma*/

/*example 03 */

/*let a = {a:10};
let b = {b:2,a:11}
Object.defineProperty(a,'d', { value:100, enumerable:false})
let target = {};
Object.assign(target, a,b);
debugger;
console.log(target);*/

/*example 04 */

/*let a = {a:10};
let b = {b:2,a:11}
let c={c:20}
Object.setPrototypeOf(b,c) // seteo dentro de prototype de b , lo que hay en c
let target = {};
Object.assign(target, a,b);//tomadirectamente las propiedades
console.log(target);*/

/**example 5 */

/*let amount = NaN;
console.log( amount === amount);
//false

console.log(Object.is(amount, amount))*/
//este metodo determina si dos valores son iguales
/*https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Object/is */

/*let amount = 0 , total = -0 ;
console.log(amount=== total);//true

console.log(Object.is(amount,total)); // false
//fixea los errores de js
*/
/*let article1 = {
	title:'LIma',
	[Symbol.for('article')]:'My article'
}

console.log(Object.getOwnPropertySymbols(article1));*/
//se le pasa el objeto
/*
	Symbol es un tipo de datos cuyos valores son únicos e immutables. Dichos valores pueden ser utilizados como identificadores (claves) de las propiedades de los objetos. 

	https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Symbol

*/

/* example 1 */
/*let eventSymbol = Symbol(' resize event');

console.log(typeof eventSymbol);// symbol*/

/* example 2 */

/*let eventSymbol = Symbol(' resize event');

console.log(eventSymbol.toString());// Symbol( resize event)
*/
/* example 3 */
/* Una de las maneras mas usadas de los symbols es para constantes */

/*let CALCULATE_EVENT_SYMBOL = Symbol('calculate event');

console.log(CALCULATE_EVENT_SYMBOL.toString());// Symbol(calculate event)*/

/* example 4 */
/*let s = Symbol('event');
let s2 = Symbol('event');
console.log(s === s2 );*/
/*
El código anterior crea dos simbolos nuevos. NO convierte la cadena en un símbolo, sino que crea un símbolo nuevo que tiene la misma descripción.*/

/* example 5*/
/*let s = Symbol.for('event');
console.log(s.toString() );//Symbol(event)*/

/* Example 6*/
/*let s = Symbol.for('event');
let s2 = Symbol.for('event');
console.log(s === s2 ); //true*/
// no tiene el id 
	/*si la decripcion del simbolo cambia como : 
		let s2 = Symbol.for('event2');
	al comparar nos botara un false*/


//example 7  */

/*let s = Symbol.for('event');
let description = Symbol.keyFor(s);
console.log(description);
*/
/*example8*/
/*
let article = {
	title:'Lima',
	[Symbol.for('article')]:'my article'	
};

let value = article[Symbol.for('article')];
console.log(value);//my article

console.log(Object.getOwnPropertyNames(article));
//['tItle']

console.log(Object.getOwnPropertySymbols(article));*/
//[Symbol(article)]
//solo se tiene acceso al string que se ha creado
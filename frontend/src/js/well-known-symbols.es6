/*
	Well-known symbols are used by built-in JavaScript algorithms. For example Symbol.iterator is utilized to iterate over items in arrays, strings, or even to define your own iterator function.
	https://rainsoft.io/detailed-overview-of-well-known-symbols/
*/

/* Example 01 */

/*let Blog = function(){
	
};
let blog = new Blog();

console.log(blog.toString()); // [object Object] 
// nos da como resultado un objeto*/

/* example 02 */
/*let Blog = function(){
	
}
Blog.prototype[Symbol.toStringTag] = 'Blog Class';

let blog = new Blog();
console.log(blog.toString()); // [object Blog Class]*/

/*let  values = [8,12,16,18];
values[Symbol.isConcatSpreadable] = false;//si agregamos esto despues concat extendido 
//[Array[4]]
console.log([].concat(values)); // se crea [8, 12, 16]*/

/* example 3 */

/*let values = [8,12,16];

values[Symbol.toPrimitive] = function (hint) {
	console.log(hint);
	return 16
}

let sum = values +100 ; 
console.log(sum);//8,12,16100*/
// con el metodo primitive 
/*default
well-known-symbols.es6:40 116*/
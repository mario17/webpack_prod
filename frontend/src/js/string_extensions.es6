/* String Extensions */

/*let title = 'Santa Barbara in Peru';
console.log(title.startsWith('Santa'));
console.log(title.endsWith('Peru'));
console.log(title.includes('eru'));*/

//El metodo startWith  define caracteres con los que una sentencia empieza o no
/*example 02 */
/*
var title = "Surfer \u{1f3c4}  Blog" ;
console.log(title);*/
//ahora acepta codigo unicode
//Surfer 🏄  Blog

/*
var Surfer = "\u{1f3c4}" ;
console.log(Surfer.length); // 2*/
/*var surfer = "\u{1f30a}\u{1f3c4}\u{1f30a}";
console.log(Array.from(surfer).length);//3
console.log(surfer);*/

/*var title  = "Mazatla\u0301n";
//console.log(title + ' ' + title.length );
//sale nuevo uniendo todo 
//console.log(title + ' ' + title.normalize().length);

console.log(title + ' ' + title.normalize().codePointAt(7).toString(16));
//6e

console.log(String.fromCodePoint(0x1f3c4)) // 🏄
/* El método estatico String.fromCodePoint() devuelve una cadena creada por una secuencia de puntos de codigo. */

/*let  title  = 'Surfer';

let output = String.raw`${title} \u{1f3c4}\n`;
//Surfer \u{1f3c4}\n
// no procede los otros caracteres
console.log(output);

let wave = '\u{1f30a}';
console.log(wave.repeat(10));*/
//🌊🌊🌊🌊🌊🌊🌊🌊🌊🌊
/* static String.raw() method is a tag function of template literals, */
/*
	https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/String/raw
*/
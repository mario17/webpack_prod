/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
/******/ 		});
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__symbols_es6__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__symbols_es6___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__symbols_es6__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__well_known_symbols_es6__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__well_known_symbols_es6___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__well_known_symbols_es6__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__object_extensions_es6__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__object_extensions_es6___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__object_extensions_es6__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__string_extensions_es6__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__string_extensions_es6___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__string_extensions_es6__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__number_extensions_es6__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__number_extensions_es6___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__number_extensions_es6__);
//Pasando parametros al constructor
/*class Project  {
	constructor(name) {
	 console.log("constructor here:  "+ name);
	}
}

class softwareProject extends Project {

}

let p = new softwareProject("Lima");
console.log(p);*/
// extends crear una subclase a partir de clases personalizadas, así como sus objetos incorporados
// interactuando con dos clases 
/*class Project  {
	constructor() {
	 	console.log("constructor here:  ");
	}
}*/

/*class softwareProject extends Project {
	constructor() {
		//super(); es necesario a la hora de extender
		//si no se referencia en la clase que esta extendiendo sale un error de contexto, ya que no sabe a que constructor obedecer
		//super es usada para llamar funciones de un objeto padre. 
	 	console.log("another constructor here:  ");
	}
}

let p = new softwareProject();
console.log(p); */

//example 3
//using methods

/*class  Project  {
	getTask() {
	 	return 50 ;
	}
}
class ProjectSoftware extends Project {
	getTask() {
	 	return super.getTask() + 6 ;
	}
}

let  p = new ProjectSoftware();
console.log(p.getTask());*/
//50

//example 4 

/*let  Project  = {
	getTask() {
	 	return 50 ;
	}
}
let ProjectSoftware ={
	getTask() {
	 	return super.getTask() + 6 ;
	}
}

Object.setPrototypeOf(ProjectSoftware, Project)
console.log(ProjectSoftware.getTask());*/

//56

//example 5  
/*
	Properties for class instances
*/
/*class Project  {
	constructor() {
		this.location = "Lima"
	}

}

class softwareProject extends Project {
	constructor() {
		super();
	}

}
let p = new softwareProject();
console.log(p.location);*/
//lima 
// se ṕueden utilziar  variables definidas en el constructor padre

//Example 6 
//same example with let 

/*class Project  {
	constructor() {
		let location = "Lima"
	}

}

class softwareProject extends Project {
	constructor() {
		super();
	}

}
let p = new softwareProject();
console.log(p.location);*/

//no toma el  contexto 
//undefined
//example 7 
//concatenando 

/*class Project  {
	constructor() {
		this.location = "Lima "
	}

}

class softwareProject extends Project {
	constructor() {
		super();
		this.location = this.location + "beauty"
	}

}
let p = new softwareProject();
console.log(p.location);*/

/*
   CLASSES : STATIC MEMBERS

*/

/*class Project  {
	// methods
	static getDefaultId(){
		return 0 ;
	}
	//static solo funciona dentro de las clases y en los metodos.
}

console.log(Project.getDefaultId());
*/

/*
   CLASSES : NEW TARGET

*/

/*class Project  {
	constructor(args) {
		// code
		console.log(new.target);//es una function
		//hace referencia al constructor directo
	}

	// methods
}
let p = new Project();*/

/*class Project  {
	constructor(args) {
		console.log(new.target , "log");//es una function
	}
}

class softProject extends Project {


	// methods
}
let p = new softProject();
/*constructor(args) {
		super();
	}*/ 

/*class Project {
	constructor() {
		console.log(new.target.getDefault());
	}
}

class softwareProject extends Project {
	static getDefault(){return 99;}
}

let p = new softwareProject();*/





//console.log(symbols);

/***/ },
/* 1 */
/***/ function(module, exports) {

/*
	NUmber extensions
*/

/*console.log(Number.parseInt === parseInt);
//true

console.log(Number.parseFloat === parseFloat);*/
//true
/*
let s = 'NaN';
console.log(isNaN(s));//ftrue porque transforma el string 
console.log(Number.isNaN(s));// Number toma el tipado , muy importante.
*/
/*let s = '8000';
console.log(isFinite(s));//ftrue porque transforma el string 
console.log(Number.isFinite(s));*/
/*
let sum = 402.5;
console.log(Number.isInteger(sum));*/

/*console.log(Number.isInteger(NaN));
console.log(Number.isInteger(Infinity));
console.log(Number.isInteger(undefined));
console.log(Number.isInteger(3));*/

//example 04 

let a = Math.pow(2,53) - 1; 
//La función  Math.pow() devuelve la  base elevada al exponente , esto es, baseexponente.
console.log(Number.isSafeInteger(a));//true
a = Math.pow(2,53);
console.log(Number.isSafeInteger(a),a);//false

/***/ },
/* 2 */
/***/ function(module, exports) {

/* 
	Object  extension
*/

/* Example 01*/

/*let a = {
	x:1
};
let b = {
	y:2
}
Object.setPrototypeOf(a,b);

console.log(a.y);2 */ 
//crea un prototype dentro de a con el valor de b 
/*example 02*/
/*let a = {a:10};
let b = {b:2,a:11}
let target = {};
Object.assign(target, a,b);//segundo y tercer parametro define el orden, en este caso a a se trae lo de b,lo sobreescribe
console.log(target); */// Object {a: 1, b: 2}
/*El método Object.assign() es usado para copiar los valores de todas la propiedades propias enumerables de uno o más objetos */
/*s i la propiedad es la misma*/

/*example 03 */

/*let a = {a:10};
let b = {b:2,a:11}
Object.defineProperty(a,'d', { value:100, enumerable:false})
let target = {};
Object.assign(target, a,b);
debugger;
console.log(target);*/

/*example 04 */

/*let a = {a:10};
let b = {b:2,a:11}
let c={c:20}
Object.setPrototypeOf(b,c) // seteo dentro de prototype de b , lo que hay en c
let target = {};
Object.assign(target, a,b);//tomadirectamente las propiedades
console.log(target);*/

/**example 5 */

/*let amount = NaN;
console.log( amount === amount);
//false

console.log(Object.is(amount, amount))*/
//este metodo determina si dos valores son iguales
/*https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Object/is */

/*let amount = 0 , total = -0 ;
console.log(amount=== total);//true

console.log(Object.is(amount,total)); // false
//fixea los errores de js
*/
/*let article1 = {
	title:'LIma',
	[Symbol.for('article')]:'My article'
}

console.log(Object.getOwnPropertySymbols(article1));*/
//se le pasa el objeto

/***/ },
/* 3 */
/***/ function(module, exports) {

/* String Extensions */

/*let title = 'Santa Barbara in Peru';
console.log(title.startsWith('Santa'));
console.log(title.endsWith('Peru'));
console.log(title.includes('eru'));*/

//El metodo startWith  define caracteres con los que una sentencia empieza o no
/*example 02 */
/*
var title = "Surfer \u{1f3c4}  Blog" ;
console.log(title);*/
//ahora acepta codigo unicode
//Surfer 🏄  Blog

/*
var Surfer = "\u{1f3c4}" ;
console.log(Surfer.length); // 2*/
/*var surfer = "\u{1f30a}\u{1f3c4}\u{1f30a}";
console.log(Array.from(surfer).length);//3
console.log(surfer);*/

/*var title  = "Mazatla\u0301n";
//console.log(title + ' ' + title.length );
//sale nuevo uniendo todo 
//console.log(title + ' ' + title.normalize().length);

console.log(title + ' ' + title.normalize().codePointAt(7).toString(16));
//6e

console.log(String.fromCodePoint(0x1f3c4)) // 🏄
/* El método estatico String.fromCodePoint() devuelve una cadena creada por una secuencia de puntos de codigo. */

/*let  title  = 'Surfer';

let output = String.raw`${title} \u{1f3c4}\n`;
//Surfer \u{1f3c4}\n
// no procede los otros caracteres
console.log(output);

let wave = '\u{1f30a}';
console.log(wave.repeat(10));*/
//🌊🌊🌊🌊🌊🌊🌊🌊🌊🌊
/* static String.raw() method is a tag function of template literals, */
/*
	https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/String/raw
*/

/***/ },
/* 4 */
/***/ function(module, exports) {

/*
	Symbol es un tipo de datos cuyos valores son únicos e immutables. Dichos valores pueden ser utilizados como identificadores (claves) de las propiedades de los objetos. 

	https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Symbol

*/

/* example 1 */
/*let eventSymbol = Symbol(' resize event');

console.log(typeof eventSymbol);// symbol*/

/* example 2 */

/*let eventSymbol = Symbol(' resize event');

console.log(eventSymbol.toString());// Symbol( resize event)
*/
/* example 3 */
/* Una de las maneras mas usadas de los symbols es para constantes */

/*let CALCULATE_EVENT_SYMBOL = Symbol('calculate event');

console.log(CALCULATE_EVENT_SYMBOL.toString());// Symbol(calculate event)*/

/* example 4 */
/*let s = Symbol('event');
let s2 = Symbol('event');
console.log(s === s2 );*/
/*
El código anterior crea dos simbolos nuevos. NO convierte la cadena en un símbolo, sino que crea un símbolo nuevo que tiene la misma descripción.*/

/* example 5*/
/*let s = Symbol.for('event');
console.log(s.toString() );//Symbol(event)*/

/* Example 6*/
/*let s = Symbol.for('event');
let s2 = Symbol.for('event');
console.log(s === s2 ); //true*/
// no tiene el id 
	/*si la decripcion del simbolo cambia como : 
		let s2 = Symbol.for('event2');
	al comparar nos botara un false*/


//example 7  */

/*let s = Symbol.for('event');
let description = Symbol.keyFor(s);
console.log(description);
*/
/*example8*/
/*
let article = {
	title:'Lima',
	[Symbol.for('article')]:'my article'	
};

let value = article[Symbol.for('article')];
console.log(value);//my article

console.log(Object.getOwnPropertyNames(article));
//['tItle']

console.log(Object.getOwnPropertySymbols(article));*/
//[Symbol(article)]
//solo se tiene acceso al string que se ha creado

/***/ },
/* 5 */
/***/ function(module, exports) {

/*
	Well-known symbols are used by built-in JavaScript algorithms. For example Symbol.iterator is utilized to iterate over items in arrays, strings, or even to define your own iterator function.
	https://rainsoft.io/detailed-overview-of-well-known-symbols/
*/

/* Example 01 */

/*let Blog = function(){
	
};
let blog = new Blog();

console.log(blog.toString()); // [object Object] 
// nos da como resultado un objeto*/

/* example 02 */
/*let Blog = function(){
	
}
Blog.prototype[Symbol.toStringTag] = 'Blog Class';

let blog = new Blog();
console.log(blog.toString()); // [object Blog Class]*/

/*let  values = [8,12,16,18];
values[Symbol.isConcatSpreadable] = false;//si agregamos esto despues concat extendido 
//[Array[4]]
console.log([].concat(values)); // se crea [8, 12, 16]*/

/* example 3 */

/*let values = [8,12,16];

values[Symbol.toPrimitive] = function (hint) {
	console.log(hint);
	return 16
}

let sum = values +100 ; 
console.log(sum);//8,12,16100*/
// con el metodo primitive 
/*default
well-known-symbols.es6:40 116*/

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(0);


/***/ }
/******/ ]);
//# sourceMappingURL=bundle.js.map